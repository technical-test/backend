package com.technicaltest.backend.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.technicaltest.backend.models.User;

public class AuthUtil {
	
	public static String createToken(User user) {
		Algorithm algorithm = Algorithm.HMAC256("technicaltest");
		String token = JWT.create().withIssuer("auth0").withClaim("name", user.getName()).withArrayClaim("roles", user.rolesString()).sign(algorithm);
		return token;
	}
	
	private static DecodedJWT verifyToken (String token) {
		Algorithm algorithm = Algorithm.HMAC256("technicaltest");
	    JWTVerifier verifier = JWT.require(algorithm)
	        .withIssuer("auth0")
	        .build();
	    return verifier.verify(token);
	}
	
	public static String getNameToken(String token) {
		return verifyToken(token).getClaim("name").asString();
	}
	
	
	
}
