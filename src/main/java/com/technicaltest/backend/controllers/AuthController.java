package com.technicaltest.backend.controllers;

import javax.validation.Valid;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.technicaltest.backend.exceptions.BadLoginException;
import com.technicaltest.backend.exceptions.BindingResultException;
import com.technicaltest.backend.exceptions.UserNotFoundException;
import com.technicaltest.backend.models.User;
import com.technicaltest.backend.repositories.UserRepository;
import com.technicaltest.backend.utils.AuthUtil;

@RestController
public class AuthController {

	@Autowired
	private UserRepository userRepository;

	@PostMapping("/auth")
	public String auth(@Valid @RequestBody User user, BindingResult result) {
		
		if (result.hasErrors()) {
			throw new BindingResultException("Binding Error Message", result);
		}

		User uDB = this.userRepository.findByName(user.getName())
				.orElseThrow(() -> new UserNotFoundException(user.getName()));

		if (!BCrypt.checkpw(user.getPassword(), uDB.getPassword()))
			throw new BadLoginException();

		return AuthUtil.createToken(uDB);

	}

}
