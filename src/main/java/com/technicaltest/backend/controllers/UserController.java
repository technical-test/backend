package com.technicaltest.backend.controllers;

import java.util.ArrayList;

import java.util.List;

import javax.validation.Valid;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.technicaltest.backend.exceptions.BindingResultException;
import com.technicaltest.backend.exceptions.RoleNotFoundException;
import com.technicaltest.backend.exceptions.UnauthorizedException;
import com.technicaltest.backend.exceptions.UserAlreadyExistsException;
import com.technicaltest.backend.exceptions.UserNotFoundException;
import com.technicaltest.backend.models.Role;
import com.technicaltest.backend.models.User;
import com.technicaltest.backend.repositories.RoleRepository;
import com.technicaltest.backend.repositories.UserRepository;
import com.technicaltest.backend.utils.AuthUtil;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;

	@GetMapping
	public List<User> getUsers() {
		return this.userRepository.findAll();
	}

	@GetMapping("{id}")
	public User getUserById(@PathVariable Integer id) {
		return this.userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
	}

	@PostMapping
	public User createUser(@Valid @RequestBody User user, BindingResult result,
			@RequestHeader(name = "Authorization") String authHeader) {
		
		if (!isAuth(authHeader))
			throw new UnauthorizedException();

		if (result.hasErrors()) {
			throw new BindingResultException("Binding Error Message", result);
		}

		List<Role> roles = checkRoles(user);

		user.setRoles(roles);
		user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
		
		if (this.userRepository.findByName(user.getName()).orElse(null) != null)
			throw new UserAlreadyExistsException(user.getName());
		
		this.userRepository.save(user);

		return user;
	}

	/**
	 * Check if roles of the user are specified. If not, add the default role (id =
	 * 1). If they're specified, check if every role exist in the DB. 
	 * 
	 * @param user
	 *            The user to check
	 * @return The valid list of roles.
	 */
	private List<Role> checkRoles(User user) {
		List<Role> roles = new ArrayList<Role>();
		if (user.getRoles() == null || user.getRoles().isEmpty()) {
			roles.add(this.roleRepository.findById(1).orElseThrow(() -> new RoleNotFoundException(1)));
		} else {
			for (Role role : user.getRoles()) {
				roles.add(this.roleRepository.findByName(role.getName())
						.orElseThrow(() -> new RoleNotFoundException(role.getName())));
			}
		}

		return roles;
	}
	
	public boolean isAuth(String token) {
		User user = this.userRepository.findByName(AuthUtil.getNameToken(token)).orElseThrow(() -> new UserNotFoundException(AuthUtil.getNameToken(token)));
		return user.isAdmin();
	}

}
