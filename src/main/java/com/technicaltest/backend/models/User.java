package com.technicaltest.backend.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="users")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@NotEmpty(message = "The username can't be empty.")
	private String name;
	
	@NotEmpty(message = "The password can't be empty.")
	private String password;
	
	@ManyToMany 
    @JoinTable(name="users_roles", joinColumns=@JoinColumn(name="user_id"), inverseJoinColumns=@JoinColumn(name="role_id"))
	private List<Role> roles;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public List<Role> getRoles() {
		return roles;
	}
	
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	//Convert the list of roles on a string array, where the string is the name of the role
	public String[] rolesString() {
		if (this.roles == null || this.roles.isEmpty())
			return null;
		
		String[] rolesString = new String[this.roles.size()];
	
		for (int i = 0; i < rolesString.length; i++) {
			rolesString[i] = this.roles.get(i).toString();
		}
		
		return rolesString;
	}
	
	//Check is the user is admin
	@JsonIgnore
	public boolean isAdmin() {
		for (Role role : roles) {
			if (role.getName().equals("admin"))
				return true;
		}
		
		return false;
	}
	
}
