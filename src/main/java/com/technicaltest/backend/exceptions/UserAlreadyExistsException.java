package com.technicaltest.backend.exceptions;

public class UserAlreadyExistsException extends RuntimeException {

	private static final long serialVersionUID = -6437847537414659894L;

	public UserAlreadyExistsException(String name) {
		super("User " + name + " already exists");
	}
}
