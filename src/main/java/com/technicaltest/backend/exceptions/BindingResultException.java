package com.technicaltest.backend.exceptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

public class BindingResultException extends RuntimeException{

	private static final long serialVersionUID = -1931469658750055342L;
	
	private Map<String, List<String>> errors;

	public BindingResultException(String message, BindingResult result) {
		super(message);
		
		this.errors = new HashMap<String, List<String>>();
		for (FieldError f : result.getFieldErrors()) {
			addError(f.getField(), f.getDefaultMessage());
		}
		
	}
	
	public Map<String, List<String>> getErrors() {
		return errors;
	}
	
	public void setErrors(Map<String, List<String>> errors) {
		this.errors = errors;
	}
	
	private void addError(String key, String value) {
		if (this.errors.get(key) != null) {
			this.errors.get(key).add(value);
		} else {
			List<String> errors = new ArrayList<>();
			errors.add(value);
			this.errors.put(key, errors);
		}
	}
	
}
