package com.technicaltest.backend.exceptions;

public class UserNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1884945990336213460L;

	public UserNotFoundException(Integer id) {
		super("Couldn't find User with ID : " + id);
	}
	
	public UserNotFoundException(String name) {
		super("Couldn't find User with Name : " + name);
	}
	
}
