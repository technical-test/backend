package com.technicaltest.backend.exceptions;

public class RoleNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -4671764994888166585L;

	public RoleNotFoundException(int id) {
		super("Couldn't find Role with ID : " + id);
	}
	
	public RoleNotFoundException(String name) {
		super("Couldn't find Role with Name : " + name);
	}
	
}
