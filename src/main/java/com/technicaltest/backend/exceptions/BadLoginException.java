package com.technicaltest.backend.exceptions;

public class BadLoginException extends RuntimeException{

	private static final long serialVersionUID = -1284913307436451867L;
	
	public BadLoginException() {
		super("The credentials are wrong");
	}

}
