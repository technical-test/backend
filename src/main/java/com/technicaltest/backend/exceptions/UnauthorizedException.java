package com.technicaltest.backend.exceptions;

public class UnauthorizedException extends RuntimeException {

	private static final long serialVersionUID = 5066521425659834071L;

	public UnauthorizedException() {
		super("You are unauthorized.");
	}
	
	
}
