package com.technicaltest.backend.exceptions;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionAdvice {
	
	@ExceptionHandler(UserNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String userNotFound(UserNotFoundException ex, HttpServletResponse response) {
		response.addHeader("context-type", "application/json; charset=utf-8");
		return ex.getMessage();
	}
	
	@ExceptionHandler(UserAlreadyExistsException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public String userExists(UserAlreadyExistsException ex, HttpServletResponse response) {
		response.addHeader("context-type", "application/json; charset=utf-8");
		return ex.getMessage();
	}
	
	
	@ExceptionHandler(RoleNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String roleNotFound(RoleNotFoundException ex, HttpServletResponse response) {
		response.addHeader("context-type", "application/json; charset=utf-8");
		return ex.getMessage();
	}
	
	@ExceptionHandler(UnauthorizedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public String unauthorized(UnauthorizedException ex, HttpServletResponse response) {
		response.addHeader("context-type", "application/json; charset=utf-8");
		return ex.getMessage();
	}
	
	@ExceptionHandler(BadLoginException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public String badLogin(BadLoginException ex, HttpServletResponse response) {
		response.addHeader("context-type", "application/json; charset=utf-8");
		return ex.getMessage();
	}
	
	
	@ExceptionHandler(BindingResultException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	public Map<String, List<String>> bindingResultException(BindingResultException ex, HttpServletResponse response) {
		response.addHeader("context-type", "application/json; charset=utf-8");
		return ex.getErrors();
	}

}
