package com.technicaltest.backend.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.technicaltest.backend.models.User;

public class AuthControllerTest extends AbstractTest{

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}
	
	@Test
	public void auth() throws Exception {
		String uri = "/auth";
		User newUser = new User();
		newUser.setName("User 1");
		newUser.setPassword("12345678");
		String inputJson = super.mapToJson(newUser);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		String tokenResponse = content;
		assertTrue(tokenResponse != null);
	}
	
}
