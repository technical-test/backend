package com.technicaltest.backend.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.technicaltest.backend.models.User;

public class UserControllerTest extends AbstractTest {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void getUsers() throws Exception {
		String uri = "/users";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		User[] users = super.mapFromJson(content, User[].class);
		assertTrue(users != null);
	}

	@Test
	public void getUsersById() throws Exception {
		String uri = "/users/1";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		User user = super.mapFromJson(content, User.class);
		assertTrue(user != null);
	}

	@Test
	public void createUser() throws Exception {
		String uri = "/users";
		User newUser = new User();
		newUser.setName("New User");
		newUser.setPassword("12345678");
		String inputJson = super.mapToJson(newUser);
		String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlcyI6WyJzdGFuZGFyZCIsImFkbWluIl0sImlzcyI6ImF1dGgwIiwibmFtZSI6IlVzZXIgMSJ9.GkiTz9uMptIQd9Uy_T2lVlGiRxghSzB_5DkP2c7mpto";
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).header("Authorization", token).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		User userResponse = super.mapFromJson(content, User.class);
		assertTrue(userResponse != null);
	}

}
