## Description

The project is created with Spring Boot 2.1.3 with Maven.

When the application starts the database is automatically created due to Spring Boot Configuration. The data is populated because Spring Boot executes the script in ```/src/main/resources/data.sql``` while it's starting.

There are only two beans in the project, they are located in the ```com.technicaltest.backend.models``` package:

```User```: It's the responsible for serialize and deserialize the users Json, and also maps the USERS table in the database.

```Role```: It's the responsible for serialize and deserialize the roles Json, and also maps the ROLES table in the database.

The application has 5 endpoints, the methods that mapping these endpoints can be found in the ```com.technicaltest.backend.controllers``` package :

```POST /auth```: Responsible for the login. Returns a JSON Web Token.

### Request:

```
    Headers:
        - Content-Type: application/json; charset=utf-8
    
    -Request Body: 
    {
        "name": "NEW USER",
        "password": "12345678",
    }
```


```GET /users```: List all the users on the database. 

```GET /users/{id}```: Return a user by a given id. 

```POST /users```: Create a user if the logged user is admin. 

### Resquest:
```
    Headers:
        - Content-Type: application/json; charset=utf-8
        - Authorization: {authToken}
    
    -Request Body: 
    {
        "name": "NEW USER",
        "password": "12345678",
	    "roles": ["admin"]
    }
```

```GET /roles```: List all the roles on the database. 

To connect to the database the project use JPA Repositories. They can be found in the ```com.technicaltest.backend.repositories``` package.

There are also differents Exceptions that are used on the endpoints methods. They can be found in the ```com.technicaltest.backend.exceptions``` package.

The class ```com.technicaltest.backend.utils.AuthUtil``` is used to code and decode the JSON Web Token.

To package the project:

```mvn clean install```

Then run with:

```java -jar ./targets/backend-0.0.1.jar```

To login for the first time:

```
Name: User 1
Password: 12345678
```
